#include "Header.h"

//deklaracja poszczeg�lnych zmiennych:
uint16_t ARRAY_ROW_SIZE = 640;
uint16_t ARRAY_COLUMN_SIZE = 480;

//tablica na poszczeg�lne piksele:
char vPixelsArray[640][480];


void portInitialize()
{
	/* 
	odczytywanie danych z kamery
	odczytywanie z pin�w D0-D7 dwa bajty - jeden piksel:
	
	640x480(VGA) - 
	
	podlaczenie zegara zewnetrznego do kamerki - XCLK
	PCLK - piksel clock - na zboczu opadajacym odczytywanie jednego bajta:
	XCLK - zewnetrzny zegar
	
	
	sumowanie kazdrgo bitu do zmeinnej:
	
	D0 - najmlodszy
	D7 - najstarszy	
	*/
			/*
	PTE2 - D0
	PTE3 - D1
	PTE6 - D2
	PTE16 - D3
	PTE17 - D4
	PTE18 - D5
	PTE19 - D6
	PTE31 - D7
		*/
	
	//inicjalizacja zegara na zewnatrz do kamery OV0706
	//zegar do portow
	SIM->SCGC5 = SIM_SCGC5_PORTD_MASK;
	SIM->SCGC5 = SIM_SCGC5_PORTE_MASK;

	//inicjalizacja port�w, ustawienie GPIO, piny D0-D7
	PORTE->PCR[2] = PORT_PCR_MUX(1); //PTE2 - D0
	PORTE->PCR[3] = PORT_PCR_MUX(1); //PTE3 - D1
	PORTE->PCR[6] = PORT_PCR_MUX(1); //PTE6 - D2
	PORTE->PCR[16] = PORT_PCR_MUX(1); //PTE16 - D3
	PORTE->PCR[17] = PORT_PCR_MUX(1); //PTE17 - D4
	PORTE->PCR[18] = PORT_PCR_MUX(1); //PTE18 - D5
	PORTE->PCR[19] = PORT_PCR_MUX(1); //PTE19 - D6
	PORTE->PCR[31] = PORT_PCR_MUX(1); //PTE31 - D7
	//ustawienie ich jako wejsc
	
	//ustawienie jako wejsc odpowiednich portow
	PTE->PDDR &= ~0b10000000000011111000000001001100;
	
	//D0 - PCLK(niebieski), D1 - XCLK(zielony), D2 - HREF(pomaranczowy), D3 - VSYNC(zolty)
	//ustawienie PCLK,HREF,VSYNC
	
	//do przerwan, wykorzystywane porty D:
	
	//zegar:
	SIM->SCGC5 = SIM_SCGC5_PORTD_MASK;
	
	PORTD->PCR[0] = PORT_PCR_MUX(1) | PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_IRQC(0x0a); //zbocze opadajace
	PORTD->PCR[2] = PORT_PCR_MUX(1) | PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_IRQC(0x09); //zbocze narastajace
	PORTD->PCR[3] = PORT_PCR_MUX(1) | PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_IRQC(0x0a); //zbocze opadajace
	
		//ustaienie jako wejscie PCLK,HREF,VSYNC
	PTD->PDDR &= ~0x0f;
	//ustawienie XCLK jako wyjscie, dostarcza clock
	PTD->PDDR |= 0x02;
	
	NVIC_SetPriority(PORTC_PORTD_IRQn, 2);
	NVIC_ClearPendingIRQ(PORTC_PORTD_IRQn);
	NVIC_EnableIRQ(PORTC_PORTD_IRQn);
}

void PORTD_IRQHandler(void)
{
	//D0 - PCLK(niebieski), D1 - XCLK(zielony), D2 - HREF(pomaranczowy), D3 - VSYNC(zolty)
	//ustawienie PCLK,HREF,VSYNC
	//przelicznie pin�w(wartosci 0,1) na warto bajt( do 255), kiedy nastepuje zbocze opadajace
	//kiedy odpowienide zbocza, wyzlawanie odpwoeidnich pin�w:
	NVIC_ClearPendingIRQ(PORTC_PORTD_IRQn);
	
	if(PORTD->ISFR & 0x03) ///przerwanie dla portu VSYNC
	{
		for(int j = 0; j < ARRAY_COLUMN_SIZE ;j++)
		{
			for(int i = 0; i < ARRAY_ROW_SIZE && (PORTD->ISFR & 0x02) ;i++) //HREF
			{
				if(PORTD->ISFR & 0x00) //PCLK
					{
						vPixelsArray[i][j] = (PTE->PDIR & 0x02) * 2^0 + (PTE->PDIR & 0x03) * 2^1 + (PTE->PDIR & 0x06)* 2^2 + (PTE->PDIR & 0x10)*2^3 + (PTE->PDIR & 0x11)*2^4 + (PTE->PDIR & 0x12)*2^5  + (PTE->PDIR & 0x13) * 2^6 + (PTE->PDIR & 0x1F) * 2^7;				 
					}
			}		
		}
	}
}


//odczytywanie pin�w
/*konfigurowanie zegara:
	XCLK - zegar systemowy(jako output)) EXTAL
	PCLK - pixel clock- zewnetrzny(jako input)
*/

/*  SCHEMAT ODCZYTYWANIA DANYCH Z KAMERY

1. ZEGAR NA CLK,VCC I GND => WTEDY WYSYLANE DANE Z D0-D7, HREF, PCLK, HSYNC, VSYNC

2. KIEDY PLCK - ZBOCZE OPADAJCACE, HREF - ZBOCZE NARASTAJACE - ODCZYTANIE JEDNEGO BAJTU Z D0-D7

3. SYGNAL HREF - ODPOWIADA ZA JEDNA LINIE, KIEDY HREF RISING EDGE -> POCZATEK LINII, HREF FALLING EDGE - KONIEC LINII

4. VSYNC - FALLING EDGE - ZACZYNA CALA RAMKE, RISING EDGE - KONCZY RAMKE, CALA TABLICA MUSI ZOSTAC PPRZECHWYCONA PRZEZ STAN NISKI VSYNC


*/

/*	
	if(vsync_falling)
	{
		
	for(int j = 0; j < ARRAY_COLUMN_SIZE & href_high;i++)
	{
		if(Href_rising)
		{
			for(int i = 0; i < ARRAY_ROW_SIZE & href_high;i++)
			{
				if(PLCK_faling)
					{
						vOneByte = D0 * 2^0 + D1 * 2^1 + D2* 2^2 + D3*2^3 + D4*2^4 + D5*2^5  + D6 * 2^6 + D7 * 2^7;
						vPixelsArray[i][j] = vOneByte;
					}
			}
		}
	}
}
	
}
*/
void Clock(void)
{
	//clock jako PIT co odpowiedni czas:
	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK;
	PIT->MCR &= ~PIT_MCR_MDIS_MASK;
	PIT->MCR |= PIT_MCR_FRZ_MASK;
	
	PIT->CHANNEL[0].LDVAL = PIT_LDVAL_TSV(2); //co 125 ns, 8 MHz
	PIT->CHANNEL[0].TCTRL &= ~PIT_TCTRL_CHN_MASK;
	PIT->CHANNEL[0].TCTRL |= PIT_TCTRL_TIE_MASK;
	
	NVIC_SetPriority(PIT_IRQn, 1);
	NVIC_ClearPendingIRQ(PIT_IRQn);
	NVIC_EnableIRQ(PIT_IRQn);
	
}

void PIT_IRQHandler(void){
	NVIC_ClearPendingIRQ(PIT_IRQn);
	if (PIT->CHANNEL[0].TFLG & PIT_TFLG_TIF_MASK)
	{
		PIT->CHANNEL[0].TFLG &= PIT_TFLG_TIF_MASK;
		//ustawic zmiane na XCLK, aby wygenerowac przebieg:
		PTD->PTOR = 0x01;
	}
}	

//przerwania jako odczytywanie zbocz narastajacych/opadajacych:

 void uart_Initialize(void){
	 SIM->SCGC4|= SIM_SCGC4_UART0_MASK;					//Clock for UART
	 SIM->SCGC5|= SIM_SCGC5_PORTA_MASK;
	 PORTA->PCR[1]|= PORT_PCR_MUX(2);						//Set MUX
	 PORTA->PCR[2]|= PORT_PCR_MUX(2);
	 
	 SIM->SOPT2 |= SIM_SOPT2_UART0SRC(2);				//Clock source
	 
	 UART0->C2 &= ~(0x4);											// Receiver enabled
	 UART0->C2 &= ~(0x8);											// Transmitter enabled
	 UART0->C4 |= 0x0F;	
	 
	 UART0->BDH = 0x00;
	 UART0->BDL = 0x0D;
	 
	 UART0->BDH &= ~UART0_BDH_SBNS_MASK;
	 UART0->C1 &= ~(UART0_C1_M_MASK|UART0_C1_PE_MASK);//no parrity control 8 bit data
	 
	 UART0->C1 &= ~UART0_C1_PE_MASK;
	 UART0->C2 |= UART0_C2_RE_MASK | UART0_C2_TE_MASK; 
} 
 
void uart_Write(uint8_t value){
	while((UART0->S1 & UART0_S1_TDRE_MASK) != UART0_S1_TDRE_MASK);
		//if(UART0->S1&UART0_S1_TDRE_MASK)
			UART0->D = value;
}
 void uart_Write_char(char value){
	 while((UART0->S1 & UART0_S1_TDRE_MASK) != UART0_S1_TDRE_MASK);
		//if(UART0->S1&UART0_S1_TDRE_MASK)
			UART0->D = value;
}

//Wysylanie tablicy na UART do komputera
void UartReading(char *vArray,int m, int n )
{	
	uart_Write_char('P');
	uart_Write_char('2');
	uart_Write(10);
	for(int i=0;i<(n*m);i++)
	{
			uart_Write(vArray[i]);
					
	}
}
