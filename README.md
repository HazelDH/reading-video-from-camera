# Reading Video from Camera

Reading video from camera OV7670 



Program created to read Video from camera OV7670. Project was compiled and 
ran on FRDM-KL46Z board with Cortex M0+. Programm was written in the Keil uVision 
environment with header for KL46Z board.

Program is designetaed to read data parallel(pins D0-D7) at specific moment
(rising/falling edges) of signals XCLK, PCLK,VSYNC,HREF- see documentation

Data was stored in the uP and transfer by UART to computer, where were processed



Further improving:
-Preprocessing data in the OV7670 camera
-using DMA to obtain data faster